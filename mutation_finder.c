#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_SEQ_LEN 10000
#define MAX_READS 1000

typedef struct {
    int position; // lokacija mutacije
    char ref_base; // baza referentnog genoma
    char alt_base; // promjenjena baza
    char type; // tip mutacije
} Mutation;

typedef struct {
    int ref_index; // lokacija/pozicija u referentnom genomu
    int seq_index; // lokacija/pozicija u sekvenci
    char *seq; // mutirana sekvenca
    char *cigar; // cigar niz
    int active; // uzimamo li sekvencu u obzir
} ReadState;

// pokretanje minimap2 analize
void run_minimap2(const char *ref_file, const char *mut_file, const char *output_file) {
    char command[512];
    // ispisi proces korisniku i spremi komandu sa dinamickim nazivima u command varijablu
    snprintf(command, sizeof(command), "minimap2 -a %s %s > %s 2> minimap2_errors.log", ref_file, mut_file, output_file);
    printf("Running command: %s\n", command);

    // pokreni komandu i docekaj rezultat
    int result = system(command);
    // javi gresku ukoliko je bilo poteskoca sa izvodenjem minimap2
    if (result != 0) {
        fprintf(stderr, "minimap2 command failed with return code %d\n", result);
    }
}

// funkcija za sortiranje rezultata minimap2 analize prema pocetnim pozicijama sekvenci pomocu samtoolsa
void sort_sam_file(const char *input_sam, const char *sorted_bam) {
    char command[512];
    snprintf(command, sizeof(command), "samtools sort -o %s %s 2> samtools_errors.log", sorted_bam, input_sam);
    printf("Running command: %s\n", command);

    int result = system(command);
    if (result != 0) {
        fprintf(stderr, "samtools sort command failed with return code %d\n", result);
    }
}

// funkcija za pretvaranje .bam datoteke u .sam datoteku pomocu samtoolsa
void convert_bam_to_sam(const char *input_bam, const char *output_sam) {
    char command[512];
    snprintf(command, sizeof(command), "samtools view -h -o %s %s 2> samtools_errors.log", output_sam, input_bam);
    printf("Running command: %s\n", command);

    int result = system(command);
    if (result != 0) {
        fprintf(stderr, "samtools view command failed with return code %d\n", result);
    }
}

// iscitavanje mutacija i gradenje mutiranog genoma
void process_reads(ReadState *reads, int read_count, FILE *csv, const char *ref_seq, char *mutated_genome) {
    int ref_len = strlen(ref_seq);
    // brojac ucestalosti mutacija na svakoj poziciji referentnog genoma
    // ovime utvrdujemo koja mutacija ce imati prednost ukoliko ih imamo vise na istim pozicijama
    // pokazivac na lokaciju genoma
    int *mut_count = (int *)calloc(ref_len, sizeof(int));
    // alociranje memorije za pokazivace na Mutation strukture
    // ovaj niz ce se koristiti za pracenje mutacija - brojanje ucestalosti
    Mutation **mutations = (Mutation **)malloc(ref_len * sizeof(Mutation *));
    // alociranje memorije za strukturu Mutation na svakoj poziciji
    for (int i = 0; i < ref_len; i++) {
        mutations[i] = (Mutation *)malloc(MAX_READS * sizeof(Mutation));
    }

    // prikupljanje mutacija
    for (int i = 0; i < read_count; i++) {
        // iteriramo po iscitanim rezultatima minimap2 alata
        ReadState *read = &reads[i];
        // dok je sekvenca jos uvijek relevantna i nismo dosli do kraja CIGAR sekvence
        while (read->active && read->cigar[0] != '\0') {
            int len; // duljina CIGAR sekvence
            char op; // mutacija koja se dogodila na poziciji
            // ako nemamo par izgleda BrojSlovo tada nemamo deklaraciju ponasanja mutacija u sekvenci
            if (sscanf(read->cigar, "%d%c", &len, &op) != 2) {
                // oznaci sekvencu kao neaktivnu i nastavi sa izvodenjem programa
                read->active = 0;
                continue;
            }

            // iscitavamo duljinu mutacije koja je na redu
            while (isdigit(*read->cigar)) read->cigar++;
            // pomicemo pokazivac na operaciju
            read->cigar++;

            // iscitavanje mutacija
            switch (op) {
                // match/missmatch
                case 'M':
                    for (int j = 0; j < len; j++) {
                        // ako se ne podudaraju baze referentnog genoma i
                        // mutirane sekvence
                        if (ref_seq[read->ref_index] != read->seq[read->seq_index]) {
                            // iscitaj koja baza je prisutna
                            char alt_base = read->seq[read->seq_index];
                            // provjera tocnog ispisa baza
                            if (alt_base == 'A' || alt_base == 'C' || alt_base == 'G' || alt_base == 'T') {
                                // u Mutation na poziciji upisi podatke potrebne za kasnije raspoznavanje mutacije
                                Mutation *mut = &mutations[read->ref_index][mut_count[read->ref_index]++];
                                mut->position = read->ref_index + 1;
                                mut->ref_base = ref_seq[read->ref_index];
                                mut->alt_base = alt_base;
                                mut->type = 'X';
                            }
                        }
                        // pomakni indekse lokacija genoma
                        read->ref_index++;
                        read->seq_index++;
                    }
                    break;
                // insertion
                case 'I':
                    for (int j = 0; j < len; j++) {
                        // iscitaj koja baza je prisutna
                        char alt_base = read->seq[read->seq_index];
                        // provjera tocnog ispisa baza
                        if (alt_base == 'A' || alt_base == 'C' || alt_base == 'G' || alt_base == 'T') {
                            // u Mutation na poziciji upisi podatke potrebne za kasnije raspoznavanje mutacije
                            Mutation *mut = &mutations[read->ref_index][mut_count[read->ref_index]++];
                            mut->position = read->ref_index + 1;
                            mut->ref_base = '-';
                            mut->alt_base = alt_base;
                            mut->type = 'I';
                            // pobrini se za tocno pomicanje indeksa u slucaju insertiona
                            read->seq_index++;
                        }
                    }
                    break;
                // deletion
                case 'D':
                    for (int j = 0; j < len; j++) {
                        // u Mutation na poziciji upisi podatke potrebne za kasnije raspoznavanje mutacije
                        Mutation *mut = &mutations[read->ref_index][mut_count[read->ref_index]++];
                        mut->position = read->ref_index + 1;
                        mut->ref_base = ref_seq[read->ref_index];
                        mut->alt_base = '-';
                        mut->type = 'D';
                        // pobrini se za tocno pomicanje indeksa u slucaju deletiona
                        read->ref_index++;
                    }
                    break;
                // preskakanje; pomakni se za len
                case 'S':
                    read->seq_index += len;
                    break;
                // sekvence koje se ne poravnavaju i ne uzimaju u obzir
                case 'H':
                    break;
                // preskakanje; pomakni se za len
                case 'N':
                    read->ref_index += len;
                    break;
                // padding za poravnanje
                case 'P':
                    break;
                default:
                    read->active = 0;
                    break;
            }
        }
    }

    // pronalazak najucestalijih mutacija za svaku poziciju
    for (int i = 0; i < ref_len; i++) {
        // ako je broj mutacija na poziciji i veci od nula
        if (mut_count[i] > 0) {
            // inicijaliziraj varijablu za pracenje istinitosti postojanja mutacija
            int has_no_mutation = 0;
            for (int j = 0; j < read_count; j++) {
                // pronadi poziciju mutacija
                int ref_pos_in_read = i - reads[j].ref_index;
                // ako je sekvenca jos uvijek u obradi i pozicija je veca od nule, ali u granicama sekvence
                if (reads[j].active && ref_pos_in_read >= 0 && ref_pos_in_read < strlen(reads[j].seq) && ref_seq[i] == reads[j].seq[ref_pos_in_read]) {
                    // postoji mutacija
                    has_no_mutation = 1;
                    break;
                }
            }
            // ako nije pronađena niti jedna sekvenca bez mutacija na poziciji
            if (!has_no_mutation) {
                // inicijaliziraj counter za pracenje ucestalosti
                int max_count = 0;
                Mutation *best_mut = NULL;
                // iteriraj po mutacijama
                for (int j = 0; j < mut_count[i]; j++) {
                    Mutation *mut = &mutations[i][j];
                    int count = 0;
                    // prebroj koliko se puta koja mutacija pojavljuje
                    for (int k = 0; k < mut_count[i]; k++) {
                        if (mutations[i][k].alt_base == mut->alt_base && mutations[i][k].type == mut->type) {
                            count++;
                        }
                    }
                    // azuriraj najucestaliju mutaciju i broj sekvenci koje imaju tu mutaciju
                    if (count > max_count) {
                        max_count = count;
                        best_mut = mut;
                    }
                }
                // ako postoji najucestalija mutacija
                if (best_mut) {
                    // zapisi ju u datoteku csv
                    fprintf(csv, "%c,%d,%c,%c\n", best_mut->type, best_mut->position, best_mut->ref_base, best_mut->alt_base);
                    // azuriraj bazu na odgovarajucoj poziciji u mutiranom genomu
                    if (best_mut->type == 'X' || best_mut->type == 'D') {
                        mutated_genome[best_mut->position - 1] = best_mut->alt_base;
                    } else if (best_mut->type == 'I') {
                        // ako je pitanje umetanja pomakni sljedece baze za jedan indeks
                        memmove(mutated_genome + best_mut->position, mutated_genome + best_mut->position - 1, strlen(mutated_genome) - best_mut->position + 1);
                        // i upisi novu umetnutu bazu
                        mutated_genome[best_mut->position - 1] = best_mut->alt_base;
                    }
                }
            }
        }
    }

    // oslobodi memoriju zauzetu za mutacije
    for (int i = 0; i < ref_len; i++) {
        free(mutations[i]);
    }
    free(mutations);
    free(mut_count);
}


void parse_sam_file(const char *sam_file, const char *csv_file, const char *ref_seq) {
    // otvori sam i csv datoteke -> iscitaj minimap2 rezultate i pripremi datoteku u koju cemo upisivati rezultate
    FILE *sam = fopen(sam_file, "r");
    FILE *csv = fopen(csv_file, "w");
    // ako ne uspijemo prijavi gresku korisniku
    if (!sam || !csv) {
        fprintf(stderr, "Failed to open SAM or CSV file.\n");
        return;
    }

    // zaglavlje csv datoteke za lakse iscitavanje rezultata
    fprintf(csv, "Mutation,Position,RefBase,AltBase\n");

    // inciijaliziramo niz reads koji ce biti strukture ReadState
    ReadState reads[MAX_READS];
    // brojac za sekvence koje se nalaze u reads strukturi
    int read_count = 0;

    char line[1024];
    // dok mozemo iscitavati iz sam datoteke
    while (fgets(line, sizeof(line), sam)) {
        // preskoci zaglavlja
        if (line[0] == '@') continue;

        // incijaliziramo pokazivac na prvi token u stringu
        char *token = strtok(line, "\t");
        // inicijalizacija ostalih parametara sam datoeke
        int field = 0, pos = 0;
        int flag = 0;
        const char *cigar = NULL;
        const char *seq = NULL;

        // dok iteriramo po tokenima
        while (token) {
            if (field == 1) {
                flag = atoi(token);
            } else if (field == 3) {
                pos = atoi(token);
            } else if (field == 5) {
                cigar = token;
            } else if (field == 9) {
                seq = token;
            }
            token = strtok(NULL, "\t");
            field++;
        }

        // provjera ispravnosti iscitavanja podataka
        if (!cigar || !seq || strcmp(cigar, "*") == 0 || strcmp(seq, "*") == 0) {
            continue;
        }

        // spremi iscitane podatke i pripremi polje za prihvat iduce iscitane sekvence
        ReadState *read = &reads[read_count++];
        read->ref_index = pos - 1;
        read->seq_index = 0;
        read->seq = strdup(seq);
        read->cigar = strdup(cigar);
        read->active = 1;
    }

    // alociraj memoriju za mutirani genom koji cemo izgraditi pomocu sekvenci
    char *mutated_genome = (char *)malloc((strlen(ref_seq) + 1) * sizeof(char));
    // kopiraj sadrzaj referentnog genoma u mutirani genom
    strcpy(mutated_genome, ref_seq);
    // procesiraj iscitane podatke
    process_reads(reads, read_count, csv, ref_seq, mutated_genome);

    // oslobodi memoriju zauzetu u svakoj sekvenci
    for (int i = 0; i < read_count; i++) {
        free(reads[i].seq);
        free(reads[i].cigar);
    }

    // oslobodi memoriju koju je zauzimala varijabla
    free(mutated_genome);
    // zatvori datoteke
    fclose(sam);
    fclose(csv);
}

// funkcija za citanje referentnog genoma
char* read_reference_sequence(const char *ref_file) {
    // otvaranje .fasta datoteke referentnog genoma => citanje
    FILE *ref = fopen(ref_file, "r");
    // ukoliko neuspjesno dohvatimo datoteku prekini program
    if (!ref) {
        return NULL;
    }

    // postavljamo pokazivac na kraj datoteke
    fseek(ref, 0, SEEK_END);
    // ocitanje duljine datoteke
    long length = ftell(ref);
    // pokazivac se postavlja na pocetak datoteke kako bi se pokrenulo iscitavanje
    fseek(ref, 0, SEEK_SET);

    // alociranje memorije za sadrzaj datoteke
    char *buffer = (char *)malloc(length + 1);
    // ukoliko racunalo nema potrebnu memoriju prekini izvodenje programa
    if (!buffer) {
        fclose(ref);
        return NULL;
    }

    // pokazivac s kojim ispunjavamo buffer sa sadrzajem datoteke
    char *p = buffer;
    // niz koji predstavlja liniju procitanih podataka
    char line[1024];
    // dok postoji iduca linija
    while (fgets(line, sizeof(line), ref)) {
        // ako je zaglavlje preskoci
        if (line[0] == '>') continue;
        // iteriraj po svakom charu
        for (char *c = line; *c != '\0'; c++) {
            // ako char nije niti pocetak niti kraj retka
            if (*c != '\n' && *c != '\r') {
                // spremi char u buffer i nastavi sa analizom
                *p++ = *c;
            }
        }
    }

    // dodaj null terminator za kraj sadrzaja
    *p = '\0';

    // zatvori datoteku
    fclose(ref);
    // vrati sadrzaj
    return buffer;
}

// glavna funkcija
int main(int argc, char *argv[]) {
    // u komandnoj liniji od korisnika ocekujemo 5 datoteka
    // ako ih ne prilozi ispisujemo gresku i prekidamo program
    if (argc != 5) {
        fprintf(stderr, "Usage: %s <ref_file> <mut_file> <sam_file> <csv_file>\n", argv[0]);
        return 1;
    }

    // spremamo imena datoteka u varijable
    const char *ref_file = argv[1];
    const char *mut_file = argv[2];
    const char *sam_file = argv[3];
    const char *csv_file = argv[4];

    // iscitavanje referentnog genoma
    char *ref_seq = read_reference_sequence(ref_file);
    // ako ref_seq ne postoji tj. nije tocno iscitan prekini program
    if (!ref_seq) {
        return 1;
    }

    // funkcija za pokretanje minimap2 analize
    run_minimap2(ref_file, mut_file, sam_file);

    // sortiranje mutacijskih sekvenci po pocetnoj poziciji
    const char *sorted_bam_file = "sorted_output.bam";
    const char *sorted_sam_file = "sorted_output.sam";
    sort_sam_file(sam_file, sorted_bam_file);
    convert_bam_to_sam(sorted_bam_file, sorted_sam_file);

    // analiza sortirane .sam datoteke -> pronalazak mutacija
    parse_sam_file(sorted_sam_file, csv_file, ref_seq);

    // oslobadanje zauzete memorije
    free(ref_seq);

    return 0;
}